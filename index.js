
const express = require('express');
const PORT = process.env.PORT || 5000
const app = express();

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res, next){
  res.render('./public/index.html');
});

app.listen(PORT, function () {
  console.log(`App lancée sur le port ${PORT}!`);
});